import { Component, OnInit,Input } from '@angular/core';
import { League } from '../league';
import { Team } from '../team';
import { LeagueTable } from '../league-table';



@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.css']
})
export class LeagueComponent implements OnInit {

  constructor() { }
  @Input() league: League;

  ngOnInit() {
  }

}
