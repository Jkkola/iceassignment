export class Player {
	constructor(
		public name: string,
		public position: string,
		public num: number,
		public dob: string,
		public nationality: string,
		public contract: string
	){}

}
