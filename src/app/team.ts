import { Game } from './game';
import { Player } from './player';


export class Team {
	constructor(
		public id: number,
		public code: string,
		public name: string,
		public shortName: string,
		public crestURL: string,
		public totalWins: number,
		public totalLosses: number,
		public totalDraws: number,
		public totalGoalsFor: number,
		public totalGoalsAgainst: number,
		public games: Game[],
		public players: Player[]
	){}
}
