import { Component, OnInit} from '@angular/core';
import { League } from './league';
import { Team } from './team';
import { LeagueTable } from './league-table';
import { Game } from './game';
import { Player } from './player';
import {DataService} from './data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ICEassignment';

  leagues : League[];
  leagues2018 : League[];
  leagues2017 : League[];
  leagues2016 : League[];
  activeLeague : League;
  activeTeam : Team;
  activeRival : Team;
  current: string;
  currentSeason: string;
  currentTeamSeason: string;//Currently unused
  activeGamesS:Game[];

  constructor(private dataService:DataService){
  this.leagues = [new League(-1,"Default",-1,"",-1,-1,-1,"",[],[])]
  this.leagues2017 = [new League(-1,"Default",-1,"",-1,-1,-1,"",[],[])]
  this.leagues2016 = [];
  this.leagues2018 = [];
  this.activeLeague = this.leagues[0];
  this.activeTeam = new Team(-1,"Temp","","","",-1,-1,-1,-1,-1,[],[]);
  this.activeRival = new Team(-1,"Temp","","","",-1,-1,-1,-1,-1,[],[]);
  this.current = "event";
  this.currentSeason = "2017";
  this.currentTeamSeason = "2017";//Currently unused
  this.activeGamesS = [];
  }

  receiveActiveLeague($event){
    if($event.id != -1){
      this.activeLeague = $event;
      this.current = "event";
      console.log(this.activeLeague);
      if(this.activeLeague.id != -1 && this.activeLeague.teams.length == 0 && this.activeLeague.leagueTables.length == 0)
      {
        var cell = this.getCell(this.activeLeague);
        this.loadTeams(cell);
        this.loadLeagueTables(cell);
      }
    }
  }
  receiveActiveRival($event){
      this.activeRival = $event;
      console.log($event);
      if($event.games.length == 0 && $event.players.length == 0)
      {
        var cell = this.getCell(this.activeLeague);
        var cell2 = this.getCell2(cell,$event)
        console.log(cell,cell2);
      if(this.currentSeason == "2017")
      {
        this.loadFixturesRival(cell,cell2,"2017");
      }
      else if(this.currentSeason == "2018")
      {
        this.loadFixturesRival(cell,cell2,"2018");
      }
      else if(this.currentSeason == "2016")
      {
        this.loadFixturesRival(cell,cell2,"2016");
      }
      this.loadPlayersRival(cell,cell2);
      }
      console.log(this.activeRival);
    }

  receiveActiveTeam($event){
    this.activeTeam = $event;
    this.current = "team";
    if(this.activeTeam.games.length == 0 && this.activeTeam.players.length == 0)
    {
      var cell = this.getCell(this.activeLeague);
      var cell2 = this.getCell2(cell,this.activeTeam);
      if(this.currentSeason == "2017")
      {
        this.loadFixtures(cell,cell2,"2017");
      }
      else if(this.currentSeason == "2018")
      {
        this.loadFixtures(cell,cell2,"2018");
      }
      else if(this.currentSeason == "2016")
      {
        this.loadFixtures(cell,cell2,"2016");
      }
      this.loadPlayers(cell,cell2);
    }
    console.log(this.activeTeam);

  }

  sortGames(games:Game[])
  {
    var games1:Game[] = [];
    var games2:Game[] = [];
    for(var i = 0; i<games.length;i++)
    {
      if(games[i].status == "FINISHED")
      {
        games1.push(games[i]);
      }
      else
      {
        games2.push(games[i]);
      }
    }
    this.activeGamesS = games1.concat(games2);
  }

  ngOnInit() {
    this.loadLeagues("2017");


  }

  changeSeason(year:string)
  {
    this.currentSeason = year;
    if(year == "2017")
    {
      console.log("2017");
      this.leagues = this.leagues2017;
      this.activeLeague = this.leagues[0];
      this.current = "event";

    }
    else if(year == "2016")
    {
      console.log("2016");
      if(this.leagues2016.length == 0)
      {

        this.loadLeagues("2016");
      }
      else
      {
        this.leagues = this.leagues2016;
        this.activeLeague = this.leagues[0];
        this.current = "event";
      }

    }    
    else if(year == "2018")
    {
      console.log("2018");
      if(this.leagues2018.length == 0)
      {

        this.loadLeagues("2018");
      }
      else
      {
        this.leagues = this.leagues2018;
        this.activeLeague = this.leagues[0];
        this.current = "event";
      }

    }
  }

  loadLeagues(year:string) { this.dataService.getLeague(year).subscribe(data => {
  console.log(data);
  var i:number;
  if(year == "2017")
  {  
    for(i=0;i<data.length;i++)
    {
      this.leagues2017[i] = new League(data[i].id,data[i].caption,data[i].currentMatchday,data[i].league,data[i].numberOfGames,data[i].numberOfMatchdays,data[i].numberOfTeams,data[i].year,[],[]);

    }
    this.leagues = this.leagues2017;
  }
  if(year == "2016")
  {  
    for(i=0;i<data.length;i++)
    {
      this.leagues2016[i] = new League(data[i].id,data[i].caption,data[i].currentMatchday,data[i].league,data[i].numberOfGames,data[i].numberOfMatchdays,data[i].numberOfTeams,data[i].year,[],[]);

    }
    this.leagues = this.leagues2016;

  }
  if(year == "2018")
  {  
    for(i=0;i<data.length;i++)
    {
      this.leagues2018[i] = new League(data[i].id,data[i].caption,data[i].currentMatchday,data[i].league,data[i].numberOfGames,data[i].numberOfMatchdays,data[i].numberOfTeams,data[i].year,[],[]);

    }
    this.leagues = this.leagues2018;

  }
    this.activeLeague = this.leagues[0];
    console.log(this.activeLeague);
      this.loadTeams(0);
      this.loadLeagueTables(0);
   }
   );
  }


  loadTeams(lInt: number) { this.dataService.getTeams("" + this.leagues[lInt].id).subscribe(data => {
    var i:number;
    console.log(data);
    if(data != "error" && data.count > 0)
    {
      for(i=0;i<data.count; i++)
      {
        var idNew = this.extractTeamId(data,i);
        this.leagues[lInt].teams[i]=new Team(idNew,data.teams[i].code,data.teams[i].name,data.teams[i].shortName,data.teams[i].crestUrl,0,0,0,0,0,[],[]);
      }
    }
    else
    {
      this.leagues[lInt].teams[0]=new Team(-1,"","No teams available","","",-1,-1,-1,-1,-1,[],[]);
    }
  }
  );
  }

  loadLeagueTables(lInt: number) { this.dataService.getTable("" + this.leagues[lInt].id).subscribe(data => {
  console.log(data);
      var i:number;
      if(data.standing)
      {
        for(i=0;i<data.standing.length; i++)
        {
          this.leagues[lInt].leagueTables[i]=new LeagueTable("standing",data.standing[i].position,data.standing[i].teamName,data.standing[i].crestURI,data.standing[i].playedGames,data.standing[i].points,data.standing[i].goals,data.standing[i].goalsAgainst,data.standing[i].goalDifference,data.standing[i].wins,data.standing[i].draws,data.standing[i].losses);
        }
      }
      else if(data.standings)
      {
          this.leagues[lInt].leagueTables[0]=new LeagueTable("standings",-1,"No League Table Available","",-1,-1,-1,-1,-1,-1,-1,-1);
      } 
      else if(data == "error")
      {
          this.leagues[lInt].leagueTables[0]=new LeagueTable("error",-1,"No League Table Available","",-1,-1,-1,-1,-1,-1,-1,-1);      
      }          
   }
   );
  }

  loadPlayers(lInt: number,tInt:number) { this.dataService.getPlayers("" + this.leagues[lInt].teams[tInt].id).subscribe(data => {
      console.log(data);
      var i:number;
      if(data != "error" && data.count > 0)
      {
        for(i=0;i<data.players.length; i++)
        {
          this.leagues[lInt].teams[tInt].players[i]=new Player(data.players[i].name,data.players[i].position,data.players[i].jerseyNumber,data.players[i].dateOfBirth,data.players[i].nationality,data.players[i].contractUntil);
        }
      }
      else
      {
        this.leagues[lInt].teams[tInt].players[0] = new Player("error","",-1,"","","");
      }
      console.log(this.activeTeam);

   }
   );
  }
    loadPlayersRival(lInt: number,tInt:number) { this.dataService.getPlayers("" + this.leagues[lInt].teams[tInt].id).subscribe(data => {
      console.log(data);
      var i:number;
      if(data != "error" && data.count > 0)
      {
        for(i=0;i<data.players.length; i++)
        {
          this.leagues[lInt].teams[tInt].players[i]=new Player(data.players[i].name,data.players[i].position,data.players[i].jerseyNumber,data.players[i].dateOfBirth,data.players[i].nationality,data.players[i].contractUntil);
        }
      }
      else
      {
        this.leagues[lInt].teams[tInt].players[0] = new Player("error","",-1,"","","");
      }
      console.log(this.activeRival);

   }
   );
  }

  loadFixtures(lInt: number,tInt:number,year:string) { this.dataService.getFixtures("" + this.leagues[lInt].teams[tInt].id,year).subscribe(data => {
  console.log(data);
      var i:number;
      if(data != "error")
      {
        for(i=0;i<data.fixtures.length; i++)
        {
          var idHome = this.extractTeamIdFix(data,i,"home");
          var idAway = this.extractTeamIdFix(data,i,"away");
          var idSelf = this.extractTeamIdFix(data,i,"self");
          var idLeague = this.extractTeamIdFix(data,i,"league");
          var leagueIndex = this.getCellById(idLeague);
          if(leagueIndex != -1)
          {
            var league = this.leagues[leagueIndex].name;
          }
          else
          {
            var league = "Unknown League";
          }
          var result = this.calcWinner(data.fixtures[i].result.goalsAwayTeam,data.fixtures[i].result.goalsHomeTeam);
          var winnerId:number;
          var winnerName:string;
          if(result == "Home")
          {
            winnerId = idHome;
            winnerName = data.fixtures[i].homeTeamName;
          }
          else if(result == "Away")
          {
            winnerId = idAway;
            winnerName = data.fixtures[i].awayTeamName;
          }
          else
          {
            winnerId = -1;
            winnerName = "Draw";
          }
          this.addWinsAndLosses(idAway,idHome,result);
          if(idHome == this.activeTeam.id){
            this.addGoals(data.fixtures[i].result.goalsHomeTeam,data.fixtures[i].result.goalsAwayTeam);
          }
          else if(idAway == this.activeTeam.id)
          { 
            this.addGoals(data.fixtures[i].result.goalsAwayTeam,data.fixtures[i].result.goalsHomeTeam);
          }

          this.leagues[lInt].teams[tInt].games[i]=new Game(data.fixtures[i].status,idSelf,data.fixtures[i].matchday,idHome,idAway,data.fixtures[i].awayTeamName,data.fixtures[i].homeTeamName,data.fixtures[i].result.goalsAwayTeam,data.fixtures[i].result.goalsHomeTeam,data.fixtures[i].date.substring(0,10),idLeague,league,result,winnerId,winnerName,data.season);
        }
        this.leagues[lInt].teams[tInt].games = this.leagues[lInt].teams[tInt].games.slice().reverse()
      }
      else
      {
        this.leagues[lInt].teams[tInt].games[0] = new Game("error",-1,-1,-1,-1,"","",-1,-1,"",-1,"","Error",-1,"","")
      }
      console.log(this.activeTeam);
      this.sortGames(this.activeTeam.games);
      console.log(this.activeGamesS);      
   }
   );
  }

    loadFixturesRival(lInt: number,tInt:number,year:string) { this.dataService.getFixtures("" + this.leagues[lInt].teams[tInt].id,year).subscribe(data => {
  console.log(data);
      var i:number;
      if(data != "error")
      {
        for(i=0;i<data.fixtures.length; i++)
        {
          var idHome = this.extractTeamIdFix(data,i,"home");
          var idAway = this.extractTeamIdFix(data,i,"away");
          var idSelf = this.extractTeamIdFix(data,i,"self");
          var idLeague = this.extractTeamIdFix(data,i,"league");
          var leagueIndex = this.getCellById(idLeague);
          if(leagueIndex != -1)
          {
            var league = this.leagues[leagueIndex].name;
          }
          else
          {
            var league = "Unknown League";
          }
          var result = this.calcWinner(data.fixtures[i].result.goalsAwayTeam,data.fixtures[i].result.goalsHomeTeam);
          var winnerId:number;
          var winnerName:string;
          if(result == "Home")
          {
            winnerId = idHome;
            winnerName = data.fixtures[i].homeTeamName;
          }
          else if(result == "Away")
          {
            winnerId = idAway;
            winnerName = data.fixtures[i].awayTeamName;
          }
          else
          {
            winnerId = -1;
            winnerName = "Draw";
          }
          this.addWinsAndLossesRival(idAway,idHome,result);
          if(idHome == this.activeRival.id){
            this.addGoalsRival(data.fixtures[i].result.goalsHomeTeam,data.fixtures[i].result.goalsAwayTeam);
          }
          else if(idAway == this.activeRival.id)
          { 
            this.addGoalsRival(data.fixtures[i].result.goalsAwayTeam,data.fixtures[i].result.goalsHomeTeam);
          }

          this.leagues[lInt].teams[tInt].games[i]=new Game(data.fixtures[i].status,idSelf,data.fixtures[i].matchday,idHome,idAway,data.fixtures[i].awayTeamName,data.fixtures[i].homeTeamName,data.fixtures[i].result.goalsAwayTeam,data.fixtures[i].result.goalsHomeTeam,data.fixtures[i].date.substring(0,10),idLeague,league,result,winnerId,winnerName,data.season);
        }
        this.leagues[lInt].teams[tInt].games = this.leagues[lInt].teams[tInt].games.slice().reverse()
      }
      else
      {
        this.leagues[lInt].teams[tInt].games[0] = new Game("error",-1,-1,-1,-1,"","",-1,-1,"",-1,"","Error",-1,"","")
      }
      console.log(this.activeRival);      
   }
   );
  }

  getCell(league: League)
  {
    var i:number;
    var cell:number = -1;
    for(i = 0; i<this.leagues.length;i++)
    {
      if(league.id == this.leagues[i].id)
      {
        cell = i;
      }
    }
    return cell;
  }

  getCellById(id: number)
  {
    var i:number;
    var cell:number = -1;
    for(i = 0; i<this.leagues.length;i++)
    {
      if(id == this.leagues[i].id)
      {
        cell = i;
      }
    }
    return cell;
  }

  getCell2(cellL: number,team: Team)
  {
    var i:number;
    var cell:number = -1;
    for(i = 0; i<this.leagues[cellL].teams.length;i++)
    {
      if(team.id == this.leagues[cellL].teams[i].id)
      {
        cell = i;
      }
    }
    return cell;
  }

  extractTeamId(data:any,index:number)
  {
    var url = data.teams[index]._links.self.href;
    var idString = url.substring(38);
    var id = +idString;
    return id;
  }
  extractTeamIdFix(data:any,index:number,type:string)
  {
    if(type == "away")
    {
      var url = data.fixtures[index]._links.awayTeam.href;
      var idString = url.substring(38);
      var id = +idString;
      return id;
    }
    else if(type == "home")
    {
      var url = data.fixtures[index]._links.homeTeam.href;
      var idString = url.substring(38);
      var id = +idString;
      return id;
    }
    else if(type == "self")
    {
      var url = data.fixtures[index]._links.self.href;
      var idString = url.substring(41);
      var id = +idString;
      return id;    
    }
    else if(type == "league")
    {
      var url = data.fixtures[index]._links.competition.href;
      var idString = url.substring(45);
      var id = +idString;
      return id;    
    }
  }

  calcWinner(away:number,home:number)
  {
    var result = "";
    if(away > home)
    {
      result = "Away";
    }
    else if(away < home)
    {
      result = "Home";
    }
    else
    {
      result = "Draw";
    }
    return result;
  }

  addWinsAndLosses(idAway:number,idHome:number,result:string)
  {
    if(result == "Home")
    {
      if(this.activeTeam.id == idHome)
      {
        this.activeTeam.totalWins++;
      }
      else
      {
        this.activeTeam.totalLosses++;
      }
    }
    else if(result == "Away")
    {
      if(this.activeTeam.id == idAway)
      {
        this.activeTeam.totalWins++;
      }
      else
      {
        this.activeTeam.totalLosses++;
      }
    }
    else
    {
      this.activeTeam.totalDraws++;
    }
  }

  addWinsAndLossesRival(idAway:number,idHome:number,result:string)
  {
    if(result == "Home")
    {
      if(this.activeRival.id == idHome)
      {
        this.activeRival.totalWins++;
      }
      else
      {
        this.activeRival.totalLosses++;
      }
    }
    else if(result == "Away")
    {
      if(this.activeRival.id == idAway)
      {
        this.activeRival.totalWins++;
      }
      else
      {
        this.activeRival.totalLosses++;
      }
    }
    else
    {
      this.activeRival.totalDraws++;
    }
  }

  addGoals(goalsFor:number,goalsAgainst:number)
  {
    this.activeTeam.totalGoalsFor = this.activeTeam.totalGoalsFor + goalsFor; 
    this.activeTeam.totalGoalsAgainst = this.activeTeam.totalGoalsAgainst + goalsAgainst;
  }
  addGoalsRival(goalsFor:number,goalsAgainst:number)
  {
    this.activeRival.totalGoalsFor = this.activeRival.totalGoalsFor + goalsFor; 
    this.activeRival.totalGoalsAgainst = this.activeRival.totalGoalsAgainst + goalsAgainst;
  }


}
