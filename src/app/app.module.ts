import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { LeagueListComponent } from './league-list/league-list.component';
import { LeagueComponent } from './league/league.component';
import {DataService} from './data.service';
import { TeamDisplayComponent } from './team-display/team-display.component';


@NgModule({
  declarations: [
    AppComponent,
    LeagueListComponent,
    LeagueComponent,
    TeamDisplayComponent
  ],
  imports: [
    BrowserModule, HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
