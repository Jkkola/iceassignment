export class Game {	
constructor(
	public status: string,
	public id: number,
	public matchDay: number,
	public homeId: number,
	public awayId: number,
	public awayTeam: string,
	public homeTeam: string,
	public scoreAway: number,
	public scoreHome: number,
	public date: string,
	public leagueId: number,
	public league: string,
	public result: string,
	public winnerId: number,
	public winnerName: string,
	public season: string
	){}
}
