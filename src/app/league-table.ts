export class LeagueTable {
constructor(
	public type: string,
	public position: number,
	public teamName: string,
	public crestURI: string,
	public playedGames: number,
	public points: number,
	public goals: number,
	public goalsAgainst: number,
	public goalDifference: number,
	public wins: number,
	public draws: number,
	public losses: number
	){}
}

