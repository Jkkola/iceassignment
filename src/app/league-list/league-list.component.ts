import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { League } from '../league';
import { Team } from '../team';
import { LeagueTable } from '../league-table';


@Component({
  selector: 'app-league-list',
  templateUrl: './league-list.component.html',
  styleUrls: ['./league-list.component.css']
})
export class LeagueListComponent implements OnInit {

  @Input() activeLeague : League;
  activeTeam : Team;
  current : string ="league";
  @Input() leagues: League[];
  @Output() activeEvent = new EventEmitter<League>();
  @Output() activeTEvent = new EventEmitter<Team>();
  constructor() { 
  	  this.activeLeague = new League(0,"Default",-1,"",-1,-1,-1,"",[],[]);
  	  this.activeTeam = new Team(-1,"Temp","","","",-1,-1,-1,-1,-1,[],[]);
  }

  ngOnInit() {
  }

  onSelect(league: League): void {
  if(this.activeLeague.id != league.id || this.current == "team")
  {
    this.activeLeague = league;
    this.current = "league";
  }
  else
  {
    this.activeLeague = new League(-1,"Default",-1,"",-1,-1,-1,"",[],[]);
    this.current = "null";
  }
  this.activeEvent.emit(this.activeLeague);
  }

  onSelectTeam(team: Team): void {
  	this.activeTeam = team;
  	this.current = "team";
  	this.activeTEvent.emit(this.activeTeam);
  }
}
