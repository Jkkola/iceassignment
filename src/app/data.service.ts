import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

const httpOptionsA = {
	headers: new HttpHeaders ({
		'X-Auth-Token': '6667ae879232485caa674182a0b3d924'
	})
	};


@Injectable()
export class DataService {

	

  constructor(private http: HttpClient) {
  	 
   }


  	getLeague(year : string) : Observable <any>
  	{


  		var urlPrefix = "http://api.football-data.org/v1/competitions/?season=" + year;
		var urlSuffix = "/leagueTable";
		
		var data = this.http.get(urlPrefix,httpOptionsA)
		.catch((err) =>{

		var errMessage:string[] = ["error"];
		return errMessage;
		});
		return data;
		
  		
  	}


  	getTeams (leagueID : string) : Observable <any>
  	{


  		var urlPrefix = "http://api.football-data.org/v1/competitions/";
		var urlSuffix = "/teams";
		
		return this.http.get<Object[]>(urlPrefix+leagueID+urlSuffix, httpOptionsA)
		.catch((err) =>{

		var errMessage:string[] = ["error"];
		return errMessage;
		}); 
	}
  	getTable(leagueID : string) : Observable <any>
  	{


  		var urlPrefix = "http://api.football-data.org/v1/competitions/";
		var urlSuffix = "/leagueTable";
		
		return this.http.get<Object[]>(urlPrefix+leagueID+urlSuffix, httpOptionsA)
		.catch((err) =>{

		var errMessage:string[] = ["error"];
		return errMessage;
		});
	}

	getFixtures(teamID : string,year:string) : Observable <any>
  	{
  		console.log(year);
  		var urlPrefix = "http://api.football-data.org/v1/teams/";
		var urlSuffix = "/fixtures/?season=" + year;
		
		return this.http.get<Object[]>(urlPrefix+teamID+urlSuffix, httpOptionsA)
		.catch((err) =>{

		var errMessage:string[] = ["error"];
		return errMessage;
		}); 
	}

	getPlayers(teamID : string) : Observable <any>
  	{
  		var urlPrefix = "http://api.football-data.org/v1/teams/";
		var urlSuffix = "/players";
		
		return this.http.get<Object[]>(urlPrefix+teamID+urlSuffix, httpOptionsA)
		.catch((err) =>{

		var errMessage:string[] = ["error"];
		return errMessage;
		}); 
	}

}