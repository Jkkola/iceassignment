import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { League } from '../league';
import { Team } from '../team';
import { LeagueTable } from '../league-table';
import { Player } from '../player';
import { Game } from '../game';




@Component({
  selector: 'app-team-display',
  templateUrl: './team-display.component.html',
  styleUrls: ['./team-display.component.css']
})
export class TeamDisplayComponent implements OnInit {

  @Input() leagues: League[];
  @Input() activeLeague : League;
  @Input() activeTeam : Team;
  @Output() activeEvent = new EventEmitter<Team>();
  current:string;
  @Input() activeRival : Team;
  @Input() activeGamesS:Game[];

  constructor() {
  	this.current = "stats";
  }

  changeScreen(type:string)
  {
  	this.current = type;
  }

  changeActiveRival(teamId:String)
  {
  	console.log(teamId);
  	for(var i = 0;i<this.activeLeague.teams.length;i++)
  	{
  		if(teamId == String(this.activeLeague.teams[i].id))
  		{
  			var team = this.activeLeague.teams[i];
  		}
  	}
  	console.log(team);
  	this.activeRival = team;
  	this.activeEvent.emit(team);
  }


  ngOnInit() {
  		if(this.activeRival.games.length == 0 && this.activeRival != this.activeTeam)
  		{
			this.activeEvent.emit(this.activeRival);
		}
  }

}
