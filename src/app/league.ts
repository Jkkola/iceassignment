import { Team } from './team';
import { LeagueTable } from './league-table';


export class League {
	constructor(
		public id: number,
		public name: string,
		public matchDay: number,
		public shortName: string,
		public nGames: number,
		public nMatchDays: number,
		public nTeams: number,
		public year: string,
		public leagueTables: LeagueTable[],
		public teams: Team[]
	){}
}
